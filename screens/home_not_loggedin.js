import React from 'react';
import {
    StyleSheet,
    Image,
    View, Text, TouchableOpacity, AsyncStorage, Modal, ScrollView
} from 'react-native';
import Navbar from "../components/navbar/navbar";

export default class Home_not_loggedin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            register: "Register",
            login: "Login",
            rules: "Read the rules",
            join: "Join a room",
            rulestext: "",
            open: false
        };
    }

    componentWillMount() {
        let self = this;
        AsyncStorage.getItem('token').then(value => {
            self.setState({token: value})
        }).then(value =>
            fetch("https://ip2-maffia.herokuapp.com/rules",
                {
                    method: 'GET',
                    headers: new Headers({
                        'Authorization': 'Bearer ' + self.state.token,
                    })
                }).then(response => {
                if (response.status === 200) {
                    response.json().then(res => {
                        self.setState({rulesText: res.data})
                    });
                }
            })
        )
    }

    render() {
        return (
            <View style={homestyles.background}>
                <Navbar/>
                <TouchableOpacity style={homestyles.register}
                                  onPress={() => this.props.navigation.navigate('registerscreen')}>
                    <Image style={homestyles.rectangleimage} source={require('../assets/images/register.png')}/>
                    <Text style={homestyles.rectangletext}>{this.state.register}</Text>
                    <Image style={homestyles.rectanglearrow} source={require('../assets/images/arrow.png')}/>
                </TouchableOpacity>
                <TouchableOpacity style={homestyles.login}
                                  onPress={() => this.props.navigation.navigate('loginscreen')}>
                    <Image style={homestyles.rectangleimage} source={require('../assets/images/login.png')}/>
                    <Text style={homestyles.rectangletext}>{this.state.login}</Text>
                    <Image style={homestyles.rectanglearrow} source={require('../assets/images/arrow.png')}/>
                </TouchableOpacity>
                <TouchableOpacity style={homestyles.rules} onPress={() => this.setState({open: true})}>
                    <Image style={homestyles.rectangleimage} source={require('../assets/images/rules.png')}/>
                    <Text style={homestyles.rectangletext}>{this.state.rules}</Text>
                    <Image style={homestyles.rectanglearrow} source={require('../assets/images/arrow.png')}/>
                </TouchableOpacity>
                <TouchableOpacity style={homestyles.about} onPress={() => this.props.navigation.navigate('joinscreen')}>
                    <Image style={homestyles.rectangleimage} source={require('../assets/images/door.png')}/>
                    <Text style={homestyles.rectangletext}>{this.state.join}</Text>
                    <Image style={homestyles.rectanglearrow} source={require('../assets/images/arrow.png')}/>
                </TouchableOpacity>
                <Modal onRequestClose={() => console.log("close modal")} visible={this.state.open}>
                    <TouchableOpacity onPress={() => this.setState({open: false})}>
                        <ScrollView>
                            <Text>{this.state.rulesText}</Text>
                        </ScrollView>
                    </TouchableOpacity>
                </Modal>
            </View>
        );
    };

}

const homestyles = StyleSheet.create({
    background: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start'
    },
    register: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        height: '23%',
        backgroundColor: "#263238"
    },
    login: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        height: '23%',
        backgroundColor: "#37474F"
    },
    rules: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        height: '22%',
        backgroundColor: "#263238"
    },
    about: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        height: '23%',
        backgroundColor: "#37474F"
    },
    rectangleimage: {
        marginLeft: 10,
        width: 50,
        height: 50
    },
    rectangletext: {
        fontSize: 40,
        color: '#ECEFF1'
    },
    rectanglearrow: {
        marginRight: 10,
        width: 50,
        height: 50
    }
});


