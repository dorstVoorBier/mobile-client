import {ScrollView, StyleSheet, TouchableOpacity, View} from "react-native";

import React from 'react';
import Votecard from "../components/card/votecard";
import SockJsClient from "react-stomp";
import {DeviceEventEmitter} from "react-native";

export default class Vote extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            random: 0,
            min: 0,
            max: 100000000000000000,
            game: this.props.gameDTO,
            url: 'https://ip2-maffia.herokuapp.com/game?token=' + this.props.token,
            dead: this.props.dead,
            username: this.props.username,
            votes: null,
            voters: [],
            role: this.props.role,
            readyCount: ''
        };
        this.handleClick = this.handleClick.bind(this);
    }

    componentWillMount() {
        this.resetVotes();
    }

    resetVotes() {
        let mapToAdd = {};
        let votedToAdd = {};
        this.state.game.players.forEach((player) => {
            mapToAdd[player] = 0;
            votedToAdd[player] = "";
        });
        this.setState({voted: votedToAdd});
        this.setState({votes: mapToAdd});
    }

    handleClick(playerName) {
        this.clientRef.sendMessage("/app/game/" + this.state.game.gameId + "/vote", '{"voter": "' + this.state.username + '","voted": [ "' + playerName + '" ]}');

    }

    renderMultiplePlayers() {
        let self = this;
        let test = [];
        for (let i = 0; i < this.state.game.players.length; i++) {
            test.push(<TouchableOpacity key={this.state.min + (Math.random() * (this.state.max - this.state.min))}
                                        onPress={() => self.handleClick(self.state.game.players[i])}><Votecard
                username={this.state.game.players[i]}
                count={this.state.votes[this.state.game.players[i]]}/></TouchableOpacity>)

        }
        return test;
    }

    handleMessage(message) {
        /**
         * @param {{voter :string}} message
         */

        if (this.state.voters[message.voter] !== "") {
            let votes = this.state.votes;
            //remove the previous vote from the voter(player)
            votes[this.state.voters[message.voter]] -= 1;
            //add the new vote from the voter(player)
            votes[message.voted] += 1;
            //setState to render the new votes
            this.setState({votes: votes});
            let voters = this.state.voters;
            //add the voters(players) their votes to the list
            voters[message.voter] = message.voted;
            //setState to keep the voters in memory
            this.setState({voters: voters});
        } else {
            let voters = this.state.voters;
            //add the voters(players) their votes to the list
            voters[message.voter] = message.voted;
            //setState to keep the voters in memory
            this.setState({voters: voters});
            let votes = this.state.votes;
            //add the new vote from the voter(player)
            votes[message.voted] += 1;
            //setState to render the new votes
            this.setState({votes: votes});
        }
    }

    readyCounter(message) {
        let count = this.state.game.players.length - message;
        this.setState({readyCount: count});
    }

    render() {
        let self = this;
        return (
            <View style={votestyles.background}>
                <SockJsClient url={this.state.url}
                              topics={["/topic/" + this.state.game.gameId + "/" + this.state.role,
                                  "/topic/" + this.state.game.gameId + "/citizen", "/topic/game/" + this.state.game.gameId + "/voteready", "/topic/game", "/topic/game/" + this.state.role]}
                              ref={(client) => {
                                  self.clientRef = client
                              }}
                              onMessage={function (message, topic) {
                                  if (topic === "/topic/game/" + self.state.game.gameId + "/voteready") {
                                      self.readyCounter(message.body);
                                  } else if (topic === "/topic/game/" + self.state.role) {
                                      DeviceEventEmitter.emit('power', {message: message.body});
                                  } else if (topic === "/topic/game") {
                                      self.setState({game: message.body});
                                  } else {
                                      self.handleMessage(message.body)
                                  }
                              }}
                />
                <ScrollView contentContainerStyle={votestyles.main}>
                    <View style={votestyles.main} pointerEvents={this.state.dead}>{this.renderMultiplePlayers()}</View>
                </ScrollView>
            </View>
        );
    };
}

const votestyles = StyleSheet.create({
    background: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start'
    },
    main: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-around'
    }
});
