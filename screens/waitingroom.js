import {Image, ListView, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import Navbar from "../components/navbar/navbar";
import React from 'react';
import {AsyncStorage} from 'react-native'
import SockJsClient from "react-stomp";
import Chat from "../components/chat/chat";
import {DeviceEventEmitter} from "react-native";
import Swiper from 'react-native-swiper';

export default class Waitingroom extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            button: "Leave",
            roomdata: null,
            url: '',
            token: '',
            username: '',
            mounted: false,
            loaded: false,
            isOpen: false,
            visible: true,
            msgicon: false
        };
        this.toggleSideMenu = this.toggleSideMenu.bind(this);
        this.changeicon = this.changeicon.bind(this);
        this.handleMessage = this.handleMessage.bind(this);
        this.leaveRoom = this.leaveRoom.bind(this);
    }

    toggleSideMenu() {
        if (!this.state.isOpen) {
            this.setState({
                isOpen: !this.state.isOpen,
            });
        } else {
            this.setState({isOpen: false})
        }

    }

    changeicon() {
        this.setState({msgicon: false})
    }

    componentDidMount() {
        let self = this;
        AsyncStorage.getItem('token').then(value => {
            self.setState({token: value})
        }).then(value => {
            self.setState({url: 'https://ip2-maffia.herokuapp.com/game?token=' + self.state.token})
        });
        AsyncStorage.getItem('username').then(value => {
            self.setState({username: value})
        }).then(value => {
            this.setState({mounted: true})
        });
        this.eventListener = DeviceEventEmitter.addListener('msg', this.handleEvent);

    }

    handleEvent = (event) => {
        this.setState({msgicon: true});
        setTimeout(() => {
            this.setState({msgicon: false})
        }, 1000);
    };

    componentWillUnmount() {
        this.eventListener.remove();
    }

    rendericon() {
        if (this.state.msgicon) {
            return <Image style={waitstyles.icon}
                          source={require('../assets/images/chat.png')}
                          fadeDuration={1000}
            />
        }
    }

    loadRoomData() {
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        var dataSource = ds;
        if (this.state.loaded) {
            var player = [];
            this.state.roomdata.players.map(function (name, index) {
                player.push(name)
            });
            dataSource = ds.cloneWithRows(player);
            return <View>
                {this.rendericon()}
                <View style={waitstyles.waiting}>
                    <View style={waitstyles.slide1}>
                        <View style={waitstyles.column1row2}>
                            <View style={waitstyles.row1}>
                                <Text style={waitstyles.name}>{this.state.roomdata.roomName}</Text>
                            </View>
                            <Text style={waitstyles.maxplayer}>Maximum
                                Players: {this.state.roomdata.maxSpelers}</Text>
                            <Text style={waitstyles.minplayer}>Minimum
                                Players: {this.state.roomdata.minSpelers}</Text>
                            <Text style={waitstyles.day}>RoundTime Day: {this.state.roomdata.secDayRoundTime}</Text>
                            <Text style={waitstyles.night}>RoundTime
                                Night: {this.state.roomdata.secNightRoundTime}</Text>
                            <Text style={waitstyles.playerlisttext}>Playerlist: </Text>
                            <ListView style={waitstyles.playerlist}
                                      automaticallyAdjustContentInsets={false}
                                      ListHeaderComponent={<Text>Playerlist</Text>}
                                      dataSource={dataSource}
                                      renderRow={(data) => <Text style={waitstyles.rowitem}>{data}</Text>}
                            />
                            <TouchableOpacity style={waitstyles.leavebutton} onPress={this.leaveRoom}>
                                <Text style={waitstyles.buttontext}>{this.state.button}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        }
    }

    leaveRoom() {
        this.clientRef.sendMessage("/app/" + this.props.navigation.state.params.id + "/leave", JSON.stringify(this.state.username));
        this.clientRef.disconnect();
        this.props.navigation.navigate('joinscreen')
    }

    renderChat() {
        if (this.state.mounted) {
            return <Chat token={this.state.token} id={this.props.navigation.state.params.id}
                         name={this.state.username}/>
        }

    }

    checkIfJoined() {
        let self = this;
        if (this.state.loaded && this.state.mounted) {
            let inRoom = false;
            this.state.roomdata.players.map(function (name, index) {
                if (name === self.state.username) {
                    inRoom = true
                }
                return null;
            });

            if (!inRoom) {
                this.props.navigation.navigate('joinscreen')
            }
        }
    }

    handleMessage(message) {
        this.props.navigation.navigate('gamescreen', {
            gameDTO: message.body,
            username: this.state.username,
            token: this.state.token
        })
    }

    render() {
        let self = this;
        if (!this.state.mounted) {
            return <View/>
        } else {
            return (
                <View style={waitstyles.background}>
                    <SockJsClient url={this.state.url}
                                  topics={['/topic/' + this.props.navigation.state.params.id + '/roominfo', '/topic/game/' + this.props.navigation.state.params.id]}
                                  ref={(client) => {
                                      self.clientRef = client
                                  }}
                                  onMessage={function (message, topic) {
                                      if (topic === '/topic/' + self.props.navigation.state.params.id + '/roominfo') {
                                          self.setState({roomdata: message.body, loaded: true})
                                      } else if (topic === '/topic/game/' + self.props.navigation.state.params.id) {
                                          self.handleMessage(message)
                                      }
                                  }
                                  }
                                  onConnect={(frame) => {
                                      self.clientRef.sendMessage("/app/" + self.props.navigation.state.params.id + "/join", JSON.stringify(self.state.username));
                                  }}
                    />
                    <Navbar/>
                    {this.checkIfJoined()}
                    <Swiper dotColor={"#ECEFF1"} activeDotColor={"#607D8B"}>
                        {this.loadRoomData()}
                        {this.renderChat()}
                    </Swiper>
                </View>
            );
        }
    };
}

const waitstyles = StyleSheet.create({
    background: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        backgroundColor: '#212121'
    },
    waiting: {
        width: '90%',
        height: '90%',
        marginTop: 20,
        backgroundColor: '#37474F',
        borderRadius: 5,
        marginLeft: 20,
    },
    row1: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    name: {
        color: '#ECEFF1',
        fontSize: 40,
        marginBottom: 20
    },
    maxplayer: {
        color: '#ECEFF1',
        fontSize: 20,
        marginBottom: 10
    },
    minplayer: {
        color: '#ECEFF1',
        fontSize: 20,
        marginBottom: 10
    },
    day: {
        color: '#ECEFF1',
        fontSize: 20,
        marginBottom: 10
    },
    night: {
        color: '#ECEFF1',
        fontSize: 20,
        marginBottom: 10
    },
    playerlisttext: {
        color: '#ECEFF1',
        fontSize: 20,
    },
    column1row2: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: '90%'
    },
    leavebutton: {
        height: '10%',
        width: 125,
        backgroundColor: '#607D8B',
        borderRadius: 5,
        marginTop: 18,
        marginRight: 15,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttontext: {
        color: '#ECEFF1',
        fontSize: 40,
    },
    dropdowncontainer: {
        width: '95%',
        marginLeft: 10
    },
    playerlist: {
        flexDirection: 'column',
        flex: 1,
        backgroundColor: '#607D8B',
        marginLeft: 10,
        height: '50%',
        minWidth: '50%'
    },
    rowitem: {
        fontSize: 17,
        borderBottomWidth: 1,
        borderBottomColor: '#212121',
        textAlign: 'center'
    },
    backimage: {
        width: 55,
        height: 50
    },
    dialog: {
        backgroundColor: '#455A64',
    },
    dialogtext: {
        fontSize: 20,
        color: '#ECEFF1',
    },
    icon: {
        width: 50,
        height: 50,
        position: 'absolute',
        top: 130,
        left: 30,
        zIndex: 1000
    }

});

