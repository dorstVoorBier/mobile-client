import {ScrollView, StyleSheet, View} from "react-native";
import Navbar from "../components/navbar/navbar";
import React from 'react';
import Room from "../components/room/room";
import {AsyncStorage} from 'react-native'

export default class Joinroom extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null,
            token: ''
        }
    }

    componentWillMount() {
        let self = this;
        //als token is ingeladen, dan pas de call gaan sturen
        AsyncStorage.getItem('token').then(value => {
            self.setState({token: value})
        }).then(value => {
            self.loadData()
        });
    }

    loadData() {
        fetch('https://ip2-maffia.herokuapp.com/rooms/getrooms', {
            method: 'GET',
            headers: new Headers({
                'Authorization': 'Bearer ' + this.state.token,
            })
        }).then(response => {
            if (response.status === 200) {
                response.json().then(res => this.setState({data: res}))
            }
        })
    }


    render() {
        return (
            <View style={joinstyles.background}>
                <Navbar/>
                <ScrollView contentContainerStyle={joinstyles.main}>
                    {this.state.data ? this.state.data.map((data, index) => {
                        return <Room key={index} name={data.roomName} count={data.aantalSpelers}
                                     size={data.maxAantalSpelers} id={data.roomId}/>
                    }) : null}
                </ScrollView>
            </View>
        );
    };
}

const joinstyles = StyleSheet.create({
    background: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start'
    },
    main: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center'
    }
});
