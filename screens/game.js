import {
    Image,
    FlatList,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    DeviceEventEmitter
} from "react-native";
import Navbar from "../components/navbar/navbar";
import React from 'react';
import Gamecard from "../components/gamecard/gamecard";
import SockJsClient from "react-stomp";
import Swiper from 'react-native-swiper';
import Chat from "../components/chat/chat";
import Vote from "./vote";

import Dialog, {DialogContent} from 'react-native-popup-dialog';

export default class Game extends React.Component {

    constructor(props) {

        super(props);
        this.state = {
            round: 'Round: X',
            time: '08:15',
            gameDTO: this.props.navigation.getParam("gameDTO", "default"),
            playerRole: null,
            loaded: false,
            dead: "auto",
            url: 'https://ip2-maffia.herokuapp.com/game?token=' + this.props.navigation.getParam("token", "default"),
            power: "",
            showPowerAlert: false,
            showDeadAlert: false,
            showWinnersAlert: false,
            alreadyDead: false

        };
        this.renderChat = this.renderChat.bind(this);
        this.showVictory = this.showVictory.bind(this);
    }

    componentWillMount() {
        this.loadData().then(() => this.setState({loaded: true}))
    }

    componentDidMount() {
        this.eventListenerReady = DeviceEventEmitter.addListener('msg', this.handleVoteReady);
        this.eventListenerPower = DeviceEventEmitter.addListener('power', this.handlePower);
    }

    componentWillUnmount() {
        this.eventListenerReady.remove();
        this.eventListenerPower.remove();
    }

    handleVoteReady = (event) => {
        this.clientRef.sendMessage("/app/game/" + this.state.gameDTO.gameId + "/voteready/" + this.props.navigation.getParam("username", "default"));
    };

    handlePower = (event) => {
        this.setState({showPowerAlert: true, power: event.message})

    };

    async loadData() {
        /**
         * @param {{gameId :string}} game
         * @param {{rolename :string}} res
         */
        await fetch('https://ip2-maffia.herokuapp.com/player/' + this.state.gameDTO.gameId + '/' + this.props.navigation.getParam("username", "default"), {
            method: 'POST',
            headers: new Headers({
                'Authorization': 'Bearer ' + this.props.navigation.getParam("token", "default"),
            })
        }).then(response => {
            if (response.status === 200) {
                response.json().then(res => {
                    this.setState({playerRole: res.rolename})
                });
            } else {
                this.setState({playerRole: "Role was not found, playername was invalid"});
            }
        })
    }

    handleMessage(message) {
        this.setState({gameDTO: message.body});
        if (!this.state.alreadyDead) {
            if (message.body.deadPlayers.hasOwnProperty(this.props.navigation.getParam("username", "default"))) {
                this.setState({dead: "none", showDeadAlert: true})
            }
        }
    }

    showVictory() {
        let self = this;
        if (this.state.gameDTO.winner !== null) {
            return <Dialog
                style={gamestyles.dialog}
                visible={true}
                onTouchOutside={() => {
                    self.props.navigation.navigate('joinscreen')
                }}
            >
                <DialogContent style={gamestyles.dialog}>
                    <Text style={gamestyles.dialogtext}>{this.state.gameDTO.winner} hebben gewonnen</Text>
                </DialogContent>
            </Dialog>
        }
    }

    renderDayOrNight() {
        if (this.state.gameDTO.day) {
            return <Image style={gamestyles.dayimage}
                          source={require('../assets/images/sun.png')}
            />
        } else return <Image style={gamestyles.dayimage}
                             source={require('../assets/images/moon.png')}
        />
    }

    renderVote() {
        let vote = <Vote gameDTO={this.state.gameDTO}
                         token={this.props.navigation.getParam("token", "default")}
                         role={this.state.playerRole}
                         dead={this.state.dead} username={this.props.navigation.getParam("username", "default")}/>;

        if (this.state.playerRole !== "citizen" && !this.state.gameDTO.day) {
            return vote
        } else if (this.state.gameDTO.day) {
            return vote
        }
    }

    renderChat = () => {
        var game = this.props.navigation.getParam("gameDTO", "default");
        var token = this.props.navigation.getParam("token", "default");
        var username = this.props.navigation.getParam("username", "default");
        let chat = <View pointerEvents={this.state.dead}><Chat token={token} id={game.gameId} name={username}/></View>;
        if (!this.state.gameDTO.day && this.state.playerRole === "maffia") {
            return chat
        } else if (!this.state.gameDTO.day && this.state.playerRole !== "maffia") {
        } else return chat
    };

    render() {
        let self = this;
        let deads = [];
        for (let name in this.state.gameDTO.deadPlayers) {
            if (this.state.gameDTO.deadPlayers.hasOwnProperty(name)) {
                let role = this.state.gameDTO.deadPlayers[name];
                deads.push({"name": name, "role": role})
            }
        }
        const menucomp = (
            <ScrollView style={menustyles.list}>
                <View style={menustyles.headermenulist}>
                    <Text style={menustyles.headitem}>Name</Text>
                    <Text style={menustyles.headitem}>Role</Text>
                    <Text style={menustyles.headitem}>State</Text>
                </View>
                <FlatList
                    data={this.state.gameDTO.players}
                    renderItem={({item}) => <View>
                        <View style={menustyles.menulist}><Text style={menustyles.item}>{item}</Text><Text
                            style={menustyles.item}>Not known</Text><Text
                            style={menustyles.item}>alive</Text></View></View>}
                    keyExtractor={(item, index) => index.toString()}
                />
                <FlatList
                    data={deads}
                    renderItem={({item}) => <View>
                        <View style={menustyles.menulist}><Text style={menustyles.item}>{item.name}</Text><Text
                            style={menustyles.item}>{item.role}</Text><Text
                            style={menustyles.item}>dead</Text></View></View>
                    }
                    keyExtractor={(item, index) => index.toString()}
                />
            </ScrollView>
        );
        if (!this.state.loaded) {
            return <View/>
        }
        return (
            <View style={gamestyles.background}>
                <Navbar/>
                {this.showVictory()}
                <Dialog
                    style={gamestyles.dialog}
                    visible={this.state.showPowerAlert}
                    onTouchOutside={() => {
                        this.setState({showPowerAlert: false});
                    }}
                >
                    <DialogContent style={gamestyles.dialog}>
                        <Text style={gamestyles.dialogtext}> {this.state.power}</Text>
                    </DialogContent>
                </Dialog>
                <Dialog
                    style={gamestyles.dialog}
                    visible={this.state.showDeadAlert}
                    onTouchOutside={() => {
                        this.setState({showDeadAlert: false, alreadyDead: true});
                    }}
                >
                    <DialogContent style={gamestyles.dialog}>
                        <Text style={gamestyles.dialogtext}>You have died!</Text>
                    </DialogContent>
                </Dialog>

                <SockJsClient url={this.state.url}
                              topics={["/topic/game"]}
                              ref={(client) => {
                                  self.clientRef = client
                              }}
                              onMessage={function (message) {
                                  self.handleMessage(message);

                              }}

                />
                <Swiper dotColor={"#ECEFF1"} activeDotColor={"#607D8B"}
                        paginationStyle={{position: 'absolute', bottom: 60}}>
                    <ScrollView contentContainerStyle={gamestyles.main}>
                        <View style={gamestyles.dead} pointerEvents={this.state.dead}>
                            <View style={gamestyles.gameheader}>
                                <TouchableOpacity>
                                    <Image style={gamestyles.friendsimage}
                                           source={require('../assets/images/friends.png')}
                                    />
                                </TouchableOpacity>
                                <Text style={gamestyles.gametext}>{this.state.gameDTO.roundCount}</Text>
                                {this.renderDayOrNight()}
                                <TouchableOpacity>
                                    <Image style={gamestyles.chatimage}
                                           source={require('../assets/images/chat.png')}
                                    />
                                </TouchableOpacity>
                            </View>
                            <Gamecard username={this.props.navigation.getParam("username", "default")}
                                      token={this.props.navigation.getParam("token", "default")}
                                      gameId={this.state.gameDTO.gameId}
                                      playerRole={this.state.playerRole}/>
                        </View>
                    </ScrollView>


                    {this.renderChat()}
                    {this.renderVote()}
                    {menucomp}
                </Swiper>
            </View>


        );


    };


}

const gamestyles = StyleSheet.create({
    background: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        backgroundColor: '#212121'
    },
    dead: {
        marginLeft: 32
    },
    main: {
        width: '100%',
        height: '110%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    gameheader: {
        width: '95%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    gametext: {
        color: '#ECEFF1',
        fontSize: 20,
        marginTop: 10
    },
    friendsimage: {
        width: 50,
        height: 50
    },
    chatimage: {
        width: 50,
        height: 50,
        marginTop: 5
    },
    dayimage: {
        width: 50,
        height: 50,
        marginTop: 5
    },
    dialog: {
        backgroundColor: '#455A64'
    },
    dialogtext: {
        fontSize: 15,
        color: '#ECEFF1'
    }
});

const menustyles = StyleSheet.create({
    item: {
        color: '#ECEFF1',
        fontSize: 15
    },
    menulist: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 20,
        marginLeft: 10,
        marginRight: 10,
        borderBottomColor: '#ECEFF1',
        borderBottomWidth: 1
    },
    list: {
        marginTop: 30
    },
    headitem: {
        color: '#ECEFF1',
        fontSize: 25
    },
    headermenulist: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 5,
        marginLeft: 10,
        marginRight: 10,
        borderBottomColor: '#ECEFF1',
        borderBottomWidth: 3
    }
});
