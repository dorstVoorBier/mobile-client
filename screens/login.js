import {Image, StyleSheet, Text, View, TouchableOpacity} from "react-native";
import {Input} from 'react-native-elements';
import {AsyncStorage} from 'react-native'
import Navbar from "../components/navbar/navbar";
import React from 'react';


export default class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "verander de state",
            password: "",
            token: null,
            logintext: 'Don\'t have an account? Sign up',
            button: 'Log in'
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit() {
        let self = this;
        fetch('https://ip2-maffia.herokuapp.com/login', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "username": self.state.username,
                "password": self.state.password
            })
        }).then((resp) => {
            if (resp.status === 200) {
                this.setState({token: resp.headers.get("Authorization").replace('Bearer ', '')});
                this.setValueUsername();
                this.setValueToken();
                this.props.navigation.navigate('joinscreen');
            } else {
                alert('Something went wrong, status code: ' + resp.status)
            }
        });
    }

    async setValueUsername() {
        await AsyncStorage.setItem('username', this.state.username);
    }

    async setValueToken() {
        await AsyncStorage.setItem('token', this.state.token);
    }

    render() {
        let self = this;
        return (
            <View style={loginstyles.background}>
                <Navbar/>
                <View style={loginstyles.main}>
                    <Image style={loginstyles.loginimage} source={require('../assets/images/login.png')}/>
                    <View style={loginstyles.logininputs}>
                        <Input style={loginstyles.logininput}
                               placeholder='Username'
                               leftIcon={{
                                   type: 'font-awesome',
                                   name: 'user',
                                   color: '#ECEFF1',
                                   marginRight: 10
                               }}
                               inputStyle={loginstyles.logininput}
                               onChangeText={(text) => self.setState({username: text})}
                        />
                        <Input style={loginstyles.logininput}
                               placeholder='Password'
                               leftIcon={{type: 'font-awesome', name: 'key', color: '#ECEFF1', marginRight: 10}}
                               inputStyle={loginstyles.logininput}
                               secureTextEntry={true}
                               onChangeText={(text) => self.setState({password: text})}
                        />
                    </View>
                    <TouchableOpacity style={loginstyles.loginbutton}
                                      onPress={self.handleSubmit}>
                        <Text style={loginstyles.buttontext}>{this.state.button}</Text>
                    </TouchableOpacity>
                    <Text style={loginstyles.logintext}
                          onPress={() => this.props.navigation.navigate('joinscreen')}>{this.state.logintext}</Text>
                </View>
            </View>
        );
    };
}

const loginstyles = StyleSheet.create({
    background: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start'
    },
    main: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center',
        height: '100%'
    },
    loginimage: {
        width: 175,
        height: 175
    },
    logintext: {
        color: '#ECEFF1',
        fontSize: 15,
        marginBottom: 50
    },
    logininputs: {
        width: '95%'
    },
    logininput: {
        color: '#ECEFF1'
    },
    loginbutton: {
        backgroundColor: '#37474F',
        borderRadius: 20
    },
    buttontext: {
        color: '#ECEFF1',
        fontSize: 20,
        height: 50,
        width: 200,
        textAlign: 'center',
        textAlignVertical: 'center'
    }
});
