import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import Navbar from "../components/navbar/navbar";
import {Input} from "react-native-elements";
import React from 'react';

export default class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: null,
            firstname: null,
            lastname: null,
            email: null,
            password: null,
            registertext: 'Already have an account? Log in',
            button: 'Sign up'
        }
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    async handleSubmit() {
        if (this.state.username === null || this.state.password === null || this.state.firstname === null || this.state.lastname === null) {
            alert('make sure to fill out all the fields')
        } else if (!this.validateEmail(this.state.email)) {
            alert('not valid email')
        } else {
            let resp = await fetch('http://ip2-maffia.herokuapp.com/users/sign-up', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "username": this.state.username,
                    "firstname": this.state.firstname,
                    "lastname": this.state.lastname,
                    "email": this.state.email,
                    "password": this.state.password
                })
            });
            if (resp.status === 200) {
                this.props.navigation.navigate('loginscreen')
            } else if (resp.status === 409) {
                alert('username already exists, try another one')
            } else {
                alert('Something went wrong, statuscode: ' + resp.status)
            }
        }
    }

    validateEmail = (email) => {
        var re = new RegExp(".+@.+\..+");
        return re.test(email);

    };

    render() {
        let self = this;
        return (
            <View style={registerstyles.background}>
                <Navbar/>
                <View style={registerstyles.main}>
                    <Image style={registerstyles.registerimage} source={require('../assets/images/register.png')}/>
                    <View style={registerstyles.registerinputs}>
                        <Input style={registerstyles.registerinput}
                               placeholder='Username'
                               leftIcon={{type: 'font-awesome', name: 'user', color: '#ECEFF1', marginRight: 10}}
                               inputStyle={registerstyles.registerinput}
                               onChangeText={(text) => {
                                   self.setState({username: text})
                               }}
                        />
                        <Input style={registerstyles.registerinput}
                               placeholder='First name'
                               leftIcon={{type: 'font-awesome', name: 'user-circle', color: '#ECEFF1', marginRight: 10}}
                               inputStyle={registerstyles.registerinput}
                               onChangeText={(text) => self.setState({firstname: text})}
                        />
                        <Input style={registerstyles.registerinput}
                               placeholder='Last name'
                               leftIcon={{type: 'font-awesome', name: 'user-circle', color: '#ECEFF1', marginRight: 10}}
                               inputStyle={registerstyles.registerinput}
                               onChangeText={(text) => self.setState({lastname: text})}
                        />
                        <Input style={registerstyles.registerinput}
                               placeholder='E-mail'
                               leftIcon={{type: 'font-awesome', name: 'envelope', color: '#ECEFF1', marginRight: 10}}
                               inputStyle={registerstyles.registerinput}
                               onChangeText={(text) => self.setState({email: text})}
                        />
                        <Input style={registerstyles.registerinput}
                               placeholder='Password'
                               leftIcon={{type: 'font-awesome', name: 'key', color: '#ECEFF1', marginRight: 10}}
                               inputStyle={registerstyles.registerinput}
                               secureTextEntry={true}
                               onChangeText={(text) => self.setState({password: text})}
                        />
                    </View>
                    <TouchableOpacity style={registerstyles.registerbutton}
                                      onPress={this.handleSubmit}>
                        <Text style={registerstyles.buttontext}>{this.state.button}</Text>
                    </TouchableOpacity>
                    <Text style={registerstyles.registertext}
                          onPress={() => this.props.navigation.navigate('loginscreen')}>{this.state.registertext}</Text>
                </View>
            </View>
        );
    };
}

const registerstyles = StyleSheet.create({
    background: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start'
    },
    main: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center',
        height: '100%'
    },
    registerimage: {
        width: 175,
        height: 175
    },
    registertext: {
        color: '#ECEFF1',
        fontSize: 15,
        marginBottom: 67
    },
    registerinputs: {
        width: '95%'
    },
    registerinput: {
        color: '#ECEFF1'
    },
    registerbutton: {
        backgroundColor: '#37474F',
        borderRadius: 20
    },
    buttontext: {
        color: '#ECEFF1',
        fontSize: 20,
        height: 50,
        width: 200,
        textAlign: 'center',
        textAlignVertical: 'center'
    }
});
