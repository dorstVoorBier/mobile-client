import React from "react";
import {
    LayoutStyle_Internal as flex,
    StyleSheet,
    Text,
    View,
    TouchableOpacity
} from "react-native";
import {withNavigation} from "react-navigation";


class Room extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            button: "Join",
            name: this.props.name,
            count: this.props.count,
            size: this.props.size,
            id: this.props.id
        };
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleSubmit() {
        this.props.navigation.navigate('waitingscreen', {id: this.state.id})
    }

    render() {
        return (
            <View style={roomstyles.room}>
                <View style={roomstyles.column1}>
                    <Text style={roomstyles.name}>{this.state.name}</Text>
                    <View style={roomstyles.column1row2}>
                        <Text style={roomstyles.playercount}>playercount:{this.state.count}/{this.state.size}</Text>
                    </View>
                </View>
                <View style={roomstyles.column2}>
                    <TouchableOpacity style={roomstyles.joinbutton} onPress={this.handleSubmit}>
                        <Text style={roomstyles.buttontext}>{this.state.button}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    };
}

const roomstyles = StyleSheet.create({
    room: {
        width: '90%',
        height: 150,
        marginTop: 15,
        backgroundColor: '#37474F',
        borderRadius: 5,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    name: {
        color: '#ECEFF1',
        fontSize: 35,
        marginLeft: 10
    },
    playercount: {
        color: '#ECEFF1',
        fontSize: 15
    },
    roomsize: {
        color: '#ECEFF1',
        fontSize: 15
    },
    column1: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    column1row2: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    joinbutton: {
        height: '100%',
        width: '100%',
        backgroundColor: '#607D8B',
        borderRadius: 5,
        marginRight: 15,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttontext: {
        color: '#ECEFF1',
        fontSize: 40,

    }
});

export default withNavigation(Room);