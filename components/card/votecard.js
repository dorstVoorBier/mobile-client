import React from "react";
import {
    StyleSheet,
    Text
} from "react-native";
import {Card} from "react-native-elements";
import {withNavigation} from "react-navigation";

class Votecard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            count: this.props.count,
            cardtext: 'VOTES: ',
            username: this.props.username
        };
    }

    render() {
        return (
            <Card
                title={this.state.username}
                image={require('../../assets/images/tommyshelby.jpg')}
                style={votecardstyles.votecard}
                titleStyle={votecardstyles.cardtitle}
                containerStyle={votecardstyles.cardcontainer}>
                <Text style={votecardstyles.cardtext}>
                    {this.state.cardtext}{this.state.count}
                </Text>
            </Card>
        );
    };
}

const votecardstyles = StyleSheet.create({
    votecard: {},
    cardtext: {
        textAlign: 'center',
        marginBottom: 10,
        color: '#ECEFF1'
    },
    cardtitle: {
        marginTop: 0,
        color: '#ECEFF1'
    },
    cardcontainer: {
        backgroundColor: '#263238',
        borderColor: '#263238',
        width: 150,
        borderRadius: 10
    },
    cardhighlight: {}
});

export default withNavigation(Votecard);