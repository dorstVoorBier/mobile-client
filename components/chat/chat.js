import React from "react";
import {
    LayoutStyle_Internal as flex,
    StyleSheet,
    Text,
    View,
    TouchableOpacity, ListView, TextInput
} from "react-native";
import {withNavigation} from "react-navigation";
import SockJsClient from "react-stomp";
import {DeviceEventEmitter} from "react-native";


class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            input: "",
            url: 'https://ip2-maffia.herokuapp.com/game?token=' + this.props.token,
            roomid: this.props.id,
            name: this.props.name
        };
        this.sendMessage = this.sendMessage.bind(this)
    }

    sendMessage() {
        this.clientRef.sendMessage("/app/" + this.state.roomid + "/allChat",
            JSON.stringify({
                'content': this.state.input,
                'playerName': this.state.name
            }));
        this.setState({input: ""})
    }

    handleMessage(message) {
        let mess = message.playerName + ": " + message.content;
        let newArray = this.state.messages.slice();
        newArray.push(mess);
        this.setState({messages: newArray});
        DeviceEventEmitter.emit('msg', {message: 'jep'});
    }

    render() {
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        var dataSource = ds;
        dataSource = ds.cloneWithRows(this.state.messages);
        return (
            <View style={waitstyles.chatmain}>
                {this.state.roomid ?
                    <SockJsClient url={this.state.url} topics={['/topic/chat/' + this.state.roomid]}
                                  ref={(client) => {
                                      this.clientRef = client
                                  }}
                                  onMessage={(message) => {
                                      this.handleMessage(message)
                                  }}
                    /> : null}
                <ListView style={waitstyles.playerlist}
                          automaticallyAdjustContentInsets={false}
                          initialListSize={1}
                          dataSource={dataSource}
                          renderRow={(data) => <Text>{data}</Text>}
                          enableEmptySections={true}
                />
                <View style={waitstyles.chatbottom}>
                    <TouchableOpacity style={waitstyles.send} onPress={this.sendMessage}>
                        <Text style={waitstyles.sendtext}>send</Text>
                    </TouchableOpacity>
                    <TextInput
                        style={waitstyles.input}
                        placeholder={this.state.message}
                        onChangeText={(text) => this.setState({input: text})}
                        value={this.state.input}
                    />
                </View>
            </View>
        );
    };
}

const waitstyles = StyleSheet.create({
    chatmain: {
        height: '75%',
        width: '90%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-end',
        marginTop: 50,
        marginLeft: 20,
        borderRadius: 5
    },
    chatbottom: {
        height: '7%',
        width: '100%',
        borderRadius: 10,
        display: 'flex',
        flexDirection: 'row'
    },
    send: {
        borderRadius: 10,
        height: '100%',
        width: '25%',
        backgroundColor: '#263238',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    sendtext: {
        color: '#ECEFF1',
        fontSize: 15,
    },
    input: {
        color: '#ECEFF1',
        fontSize: 15,
        height: '100%',
        width: '75%',
        borderRadius: 5,
        backgroundColor: '#37474F',
    },
    playerlist: {
        flexDirection: 'column',
        flex: 1,
        backgroundColor: '#607D8B',
        borderRadius: 5
    }
});

export default withNavigation(Chat);