import React from "react";
import {
    ____LayoutStyle_Internal as flex,
    Image,
    StyleSheet,
    Text,
    View,
    TouchableOpacity
} from "react-native";
import {withNavigation} from "react-navigation";

class Navbar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            titleText: "Maffia",
        };
    }

    render() {
        return (
            <View>
                <View style={navbarstyles.navbar}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('homescreen')}>
                        <Image style={navbarstyles.homeimage}
                               source={require('../../assets/images/logo.png')}
                        />
                    </TouchableOpacity>
                    <Text style={navbarstyles.titletext}>{this.state.titleText}</Text>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('joinscreen')}>
                        <Image style={navbarstyles.settingsimage}
                               source={require('../../assets/images/settings.png')}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        );
    };
}

const navbarstyles = StyleSheet.create({
    navbar: {
        display: flex,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 30,
        backgroundColor: '#455A64',
        width: '100%',
    },
    homeimage: {
        width: 50,
        height: 70,
        marginLeft: 5,
        marginTop: 5
    },
    titletext: {
        fontSize: 50,
        fontWeight: 'bold',
        color: '#ECEFF1'
    },
    settingsimage: {
        width: 50,
        height: 50,
        marginRight: 10,
        marginTop: 10
    }
});

export default withNavigation(Navbar);