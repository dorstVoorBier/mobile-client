import React from "react";
import {
    StyleSheet,
    Text,
    View
} from "react-native";
import {Button, Card, Icon} from "react-native-elements";
import {withNavigation} from "react-navigation";
import Dialog, {DialogContent} from 'react-native-popup-dialog';
import {DeviceEventEmitter} from "react-native";


class Gamecard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: this.props.username,
            playerRole: this.props.playerRole,
            visible: false
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit() {
        DeviceEventEmitter.emit('msg', {message: 'ready'});
    }

    render() {
        return (
            <View style={gamecardstyles.cardhighlight}>
                <Card
                    title={this.state.username}
                    image={require('../../assets/images/tommy.jpg')}
                    imageStyle={gamecardstyles.cardimage}
                    style={gamecardstyles.gamecard}
                    titleStyle={gamecardstyles.cardtitle}
                    containerStyle={gamecardstyles.cardcontainer}>
                    <Button
                        icon={<Icon name='format-align-justify' color='#ECEFF1'/>}
                        buttonStyle={gamecardstyles.cardbutton1}
                        title='Show role' onPress={() => this.setState({visible: true})}/>
                    <Button
                        icon={<Icon name='thumb-up' color='#ECEFF1'/>}
                        buttonStyle={gamecardstyles.cardbutton2}
                        title='Ready!'
                        onPress={this.handleSubmit}
                    />
                    <Dialog
                        style={gamecardstyles.dialog}
                        visible={this.state.visible}
                        onTouchOutside={() => {
                            this.setState({visible: false});
                        }}
                    >
                        <DialogContent style={gamecardstyles.dialog}>
                            <Text style={gamecardstyles.dialogtext}> {this.state.playerRole}</Text>
                        </DialogContent>
                    </Dialog>
                </Card>
            </View>
        );
    };
}

const gamecardstyles = StyleSheet.create({
    gamecard: {},
    cardtext: {
        textAlign: 'center',
        marginBottom: 10,
        color: '#ECEFF1'
    },
    cardtitle: {
        marginTop: 0,
        color: '#ECEFF1'
    },
    cardcontainer: {
        backgroundColor: '#263238',
        borderColor: '#263238',
        width: 300,
        height: 550,
        borderRadius: 10
    },
    cardhighlight: {},
    cardimage: {
        width: '100%',
        height: '75%'
    },
    cardbutton1: {
        marginBottom: 10,
        backgroundColor: '#455A64'
    },
    cardbutton2: {
        backgroundColor: '#455A64'
    },
    dialog: {
        backgroundColor: '#455A64'
    },
    dialogtext: {
        fontSize: 15,
        color: '#ECEFF1'
    }
});

export default withNavigation(Gamecard);