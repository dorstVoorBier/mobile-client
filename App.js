import React from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import Home_not_loggedin from "./screens/home_not_loggedin";
import {createSwitchNavigator, createAppContainer} from 'react-navigation';
import Login from "./screens/login";
import Register from "./screens/register";
import Vote from "./screens/vote";
import Game from "./screens/game";

import Joinroom from "./screens/joinroom";
import Waitingroom from "./screens/waitingroom";

export default class App extends React.Component {
    static navigationOptions = {
        header: null,
    };

  render() {
    return (
        <View style={styles.container}>
            {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
            {<AppContainer/>}
        </View>
    );
  }
}

const AppSwitchNavigator = createSwitchNavigator ({
    homescreen:{screen:Home_not_loggedin},
    loginscreen:{screen:Login},
    registerscreen:{screen:Register},
    votescreen:{screen:Vote},
    gamescreen:{screen:Game},
    joinscreen:{screen: Joinroom},
    waitingscreen:{screen: Waitingroom}
});

const AppContainer = createAppContainer(AppSwitchNavigator);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#212121',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

